package pl.edu.pwr.pp;

import java.time.LocalDateTime;
import java.util.Random;

/**
 * Created by Kamil on 2016-04-27.
 */
public class Horoskop {

    public int getLength(String name) {
        if(name == null){
            throw new IllegalArgumentException("Name cannot be null!");
        }
        return name.replaceAll("\\s+", "").length();
    }

    public int getHealthNum(String name) {
        return getLength(name) % 12;
    }

    public int getLoveNum() {
        return (LocalDateTime.now().getDayOfMonth() - 1) % 12;
    }

    public int getWorkNum() {
        return new Random().nextInt(11);
    }

    public String getHealthProphecy(String name) {
        return Przepowiednia.ZDROWIE[getHealthNum(name)];
    }

    public String getLoveProphecy() {
        return Przepowiednia.MILOSC[getLoveNum()];
    }

    public String getWorkProphecy() {
        return Przepowiednia.PRACA[getWorkNum()];
    }
}
