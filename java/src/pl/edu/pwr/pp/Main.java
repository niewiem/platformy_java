package pl.edu.pwr.pp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        System.out.println("Jak się nazywasz? (podaj swoje imię i nazwisko w jednej lini):");
        String name = getDataFromUser();

        Horoskop horoskop = new Horoskop();

        System.out.println();
        System.out.println("Twoja przepowiednia zdrowia to:\n\t" + horoskop.getHealthProphecy(name));
        System.out.println("Twoja przepowiednia miołości to:\n\t" + horoskop.getLoveProphecy());
        System.out.println("Twoja przepowiednia pracy to:\n\t" + horoskop.getWorkProphecy());

    }

    public static String getDataFromUser() {
        String readed = null;
        try (BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in))) {
            readed = bufferRead.readLine();
        } catch (IOException ex) {
            System.out.println("Problem with reading data from user.");
            ex.printStackTrace();
        }
        return readed;
    }

}
