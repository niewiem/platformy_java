package pl.edu.pwr.pp;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.lessThan;

/**
 * Created by Kamil on 2016-04-27.
 */


public class HoroskopTest {

    Horoskop horoskop;
    String firstName;
    String lastName;

    @Before
    public void setUp() {
        horoskop = new Horoskop();
        firstName = "Krzychu";
        lastName = "Kowalski";
    }

    @Test
    public void shouldReturnNumberOfCharsAndIgnoreWhiteSpaces() {
        int len = horoskop.getLength(firstName + "  \n\t " + lastName);
        assertThat("Failed, wrong chars number", len, is(equalTo(15)));

        // 0 characters
        len = horoskop.getLength("");
        assertThat("Failed, wrong chars number", len, is(equalTo(0)));
    }

    @Test
    public void shouldThrowExceptionForNullArgument() {
        try {
            horoskop.getLength(null);
        } catch (Exception e) {
            assertThat("Failed catch exception", e, is(instanceOf(IllegalArgumentException.class)));
            assertThat("Failed catch exception", e.getMessage(), is(equalTo("Name cannot be null!")));
        }
    }

    @Test
    public void shouldReturnNumberForHealth() {
        int healthNum = horoskop.getHealthNum(firstName + lastName);
        int len = (firstName.length() + lastName.length()) % 12;
        assertThat("Failed, wrong health number", healthNum, is(equalTo(len)));

        // 12%12 == 0
        len = horoskop.getHealthNum("012345678901");
        assertThat("Failed, wrong chars number", len, is(equalTo(0)));
    }

    @Test
    public void shouldReturnNumberForLove() {
        int healthNum = horoskop.getLoveNum();
        LocalDateTime date = LocalDateTime.now();
        assertThat("Failed, wrong love number", healthNum, is(equalTo((date.getDayOfMonth() - 1) % 12)));

    }

    @Test
    public void shouldReturnNumberForWork() {
        int healthNum = horoskop.getWorkNum();
        assertThat("Failed, wrong work number", healthNum, is(lessThan(12)));
    }

    /*
     * Teoretycznie test powinien się znajdować w klasie PrzepowiedniaTest ale jest to jedyny sensowny test dla tej klasy
     * i jest on powiązany bezpośrednio z tym na jakich zasadach Horoskop ma się z nim komunikować (to horoskop zakłada,
     * że jest 12 przepowiedni, przepowiednia w żaden sposób tego nie określa ).
     */
    @Test
    public void shouldBeCorrectNumberOfProphecies() {
        assertThat("Wrong number of prophecies.", Przepowiednia.MILOSC.length, is(equalTo(12)));
        assertThat("Wrong number of prophecies.", Przepowiednia.ZDROWIE.length, is(equalTo(12)));
        assertThat("Wrong number of prophecies.", Przepowiednia.PRACA.length, is(equalTo(12)));
    }

}